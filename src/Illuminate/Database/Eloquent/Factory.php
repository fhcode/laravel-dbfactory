<?php
/**
 * Class Factory
 *
 * @package Illuminate\Database\Eloquent
 */
namespace Illuminate\Database\Eloquent;
/**
 * Class Factory: for generating and using Eloquent model classes on the fly.
 *
 * Basic usage:
 *
 * <pre>
 *
 * $f = new Factory();
 *
 * // Define the named connection in the laravel application shell or the IoC first.
 * // Then associate it with your models with the following line.
 * $f->setConnection("MyConn");
 *
 * // Define any relations if there are any
 * $f->setRelation("User","hasMany","File");
 *
 * // If desired, set the relation accessor method so you can access your relation
 * // via the method of your choice.
 * $f->setRelationAccessorAlias("User","files");
 *
 * // Now generate the class and instantiate it with:
 *
 * $user = $f->create("User");
 *
 * // Access user's files:
 *
 * $files = $user->files();
 *
 * // OR
 * $files = User::find(1)->files();
 *
 * // Lots of other features. See the code.
 *
 * </pre>
 */
class Factory {

	/**
	 * @var string default namespace for factory generated classes that don't have a namespace when being created.
	 */
	protected $defaultNamespace = "Illuminate\\Database\\Eloquent\\Factory";

	/**
	 * @var string default primary key if no primary key is set for the given table
	 */
	protected $defaultPrimaryKey = 'id';

	/**
	 * @var string default base class that will be extended when creating factory models
	 */
	protected $defaultBaseClass = "\\Illuminate\\Database\\Eloquent\\Model";

	/**
	 * Named connection that will be used for all classes unless overridden for a particular class.
	 * @var string $connectionName
	 */
	protected $connectionName = null;

	/**
	 * Relations resolved from calls to $this->setRelation() and $this->setSchema();
	 *
	 * @var array $relations
	 */
	protected $relations = array();

	/**
	 * The path where files will be written when they are generated.
	 *
	 * @var string
	 */
	protected $storageLocation = "";

	/**
	 * If set to true, class files written to the storage directory will be automatically
	 * removed after they are loaded.
	 *
	 * @api
	 * @var bool
	 */
	public $bCleanupFiles = FALSE;

	/**
	 * Constructor initializes the factory.
	 * @api
	 */
	public function __construct()
	{
		$sep = DIRECTORY_SEPARATOR;
		$this->storageLocation = $sep . 'tmp' . $sep . 'eloquent_cache';
	}

	/**
	 * Sets the storage location for where files will be written when they are generated.
	 *
	 * @api
	 * @param $strPath
	 */
	public function setStorageLocation($strPath)
	{
		$this->storageLocation = $strPath;
	}

	/**
	 * Gets the storage location for where files will be written when they are generated.
	 *
	 * @api
	 * @return string
	 */
	public function getStorageLocation()
	{
		return $this->storageLocation;
	}

	/**
	 * Sets the base class to be extended instead of
	 * Illuminate\Database\Eloquent\Model
	 *
	 * @api
	 * @param mixed $mxdBaseClass
	 * @param string $strClassName if specified, only the named strClassName will extend $strBaseClass
	 */
	public function setBaseClass($mxdBaseClass, $strClassName = null)
	{
		if(!is_string($mxdBaseClass))
		{
			if(is_object($mxdBaseClass))
			{
				$mxdBaseClass = get_class($mxdBaseClass);
			}
			else
			{
				throw new EloquentFactoryException("setBaseClass says: parameter 1 must be a string or an object.");
			}
		}
		$strFullPath = $this->getFullyQualifiedClassName($strClassName);
		$strRootChar = ($mxdBaseClass[0] == "\\") ? '':"\\";
		if(is_null($strClassName))
		{
			$this->defaultBaseClass = $strRootChar . $mxdBaseClass;
		}
		$this->initRelation($strFullPath);
		$this->relations[$strFullPath]['baseclass'] = $strRootChar . $mxdBaseClass;

	}

	/**
	 * Returns the base class name that will be extended instead of
	 * Illuminate\Database\Eloquent\Model
	 * Note: If no class name is provided, the global setting will be returned.
	 *
	 * @api
	 * @param $strClassName
	 * @return string
	 */
	public function getBaseClass($strClassName = null)
	{
		$strFullPath = $this->getFullyQualifiedClassName($strClassName);
		$this->initRelation($strFullPath);
		if( is_null($strClassName) || !isset($this->relations[$strFullPath]['baseclass']) )
		{
			return $this->defaultBaseClass;
		}
		return $this->relations[$strFullPath]['baseclass'];
	}

	/**
	 * Sets the table name for the given class.
	 * If no table name is set, then the lowercase class name will be used.
	 *
	 * @api
	 * @param string $strClassName
	 * @param string $strTableName
	 */
	public function setTableName($strClassName,$strTableName)
	{
		$strFullPath = $this->getFullyQualifiedClassName($strClassName);
		$this->initRelation($strFullPath);
		$this->relations[$strFullPath]['table'] = $strTableName;
	}

	/**
	 * Returns the table name for the given class.
	 *
	 * @api
	 * @param $strClassName
	 * @return string
	 * @throws EloquentFactoryException
	 */
	public function getTableName($strClassName)
	{
		$strFullPath = $this->getFullyQualifiedClassName($strClassName);
		if(class_exists($strFullPath))
		{
			$c = new $strFullPath();
			return $c->getTable();
		}
		$this->initRelation($strFullPath);
		if(!isset($this->relations[$strFullPath]['table']))
		{
			return str_replace('\\', '', snake_case(($this->getNamespace($strFullPath,TRUE))));
		}
		return $this->relations[$strFullPath]['table'];
	}
	/**
	 * Sets the primary key for a given class.
	 *
	 * @api
	 * @param string $primaryKey
	 * @param string $strClassName
	 */
	public function setPrimaryKey($primaryKey,$strClassName = null)
	{
		if(is_null($strClassName))
		{
			$this->defaultPrimaryKey = $primaryKey;
			return;
		}
		$strFullPath = $this->getFullyQualifiedClassName($strClassName);
		$this->initRelation($strFullPath);
		$this->relations[$strFullPath]['primaryKey'] = $primaryKey;
	}

	/**
	 * Returns the primary key of the given Eloquent model.
	 *
	 * @api
	 * @param $strClassName
	 * @return string
	 */
	public function getPrimaryKey($strClassName = null)
	{
		if(is_null($strClassName))
		{
			return $this->defaultPrimaryKey;
		}
		$strFullPath = $this->getFullyQualifiedClassName($strClassName);
		if(class_exists($strFullPath))
		{
			$c = new $strFullPath();
			return $c->getKeyName();
		}
		if(!isset($this->relations[$strFullPath]))
		{
			$this->initRelation($strFullPath);
		}
		if(!isset($this->relations[$strFullPath]['primaryKey']))
		{
			return $this->defaultPrimaryKey;
		}
		return $this->relations[$strFullPath]['primaryKey'];
	}

	/**
	 * Sets the connection name for the factory or for the specified class.
	 *
	 * @api
	 * @param string $strConnectionName
	 * @param string $strClassName
	 */
	public function setConnection($strConnectionName,$strClassName = null)
	{
		$strFullPath = $this->getFullyQualifiedClassName($strClassName);
		if($strClassName)
		{
			$this->initRelation($strFullPath);
			$this->relations[$strFullPath]['connection'] = $strConnectionName;
			return;
		}
		$this->connectionName = $strConnectionName;

	}

	/**
	 * Returns the current connection for the factory.
	 * Or returns the connection defined in the schema for the given class.
	 *
	 * @api
	 * @param string $strClassName class name you want the connection for.
	 */ 
	public function getConnection($strClassName = null)
	{
		$strFullPath = $this->getFullyQualifiedClassName($strClassName);
		if( $strClassName )
		{
			if(class_exists($strFullPath))
			{
				$c = new $strFullPath();
				return $c->getConnectionName();
			}

			$this->initRelation($strFullPath);

			if(isset($this->relations[$strFullPath]['connection']))
			{
				return $this->relations[$strFullPath]['connection'];
			}
		}

		return $this->connectionName;
	}

	/**
	 * Sets the schema for all the models that can be created
	 * by this factory.
	 *
	 * @api
	 * @param array $aSchema
	 */ 
	public function setSchema($aSchema)
	{
		foreach($aSchema AS $strClassPath => $aDefinition)
		{
			$strFullPath = $this->getFullyQualifiedClassName($strClassPath);
			$this->validateTableConfig($aDefinition);
			$this->relations[$strFullPath] = $aDefinition;
		}
	}

	/**
	 * Returns the schema for all the tables.
	 *
	 * @api
	 * @return array
	 */
	public function getSchema()
	{
		return $this->relations;
	}

	/**
	 * Sets the schema for all the models that can be created
	 * by this factory.
	 *
	 * @api
	 * @param string $strClassName
	 * @param string $strType
	 * @param string $strForeignTable
	 * @param null $strForeignKey
	 * @param null $strLocalKey
	 * @param null $strRelationAccessorAlias
	 */
	public function setRelation($strClassName, $strType, $strForeignTable, $strForeignKey = null, $strLocalKey = null, $strRelationAccessorAlias = null)
	{
		$this->initRelation($strClassName,$strForeignTable);
		$strFullPath = $this->getFullyQualifiedClassName($strClassName);
		$config = $this->getClassConfig($strFullPath);

		$config[$strForeignTable] = array(
			$strType => array(
			)
		);

		if(!is_null($strForeignKey))
		{
			$config[$strForeignTable][$strType][] = $strForeignKey;
		}

		if(!is_null($strLocalKey))
		{
			if(is_null($strForeignKey))
			{
				$config[$strForeignTable][$strType][] = null;
			}
			$config[$strForeignTable][$strType][] = $strLocalKey;
		}

		if(!is_null($strRelationAccessorAlias))
		{
			$config[$strForeignTable]['relationAccessorAlias'] = $strRelationAccessorAlias;
		}

		$this->validateTableConfig($config);
		$this->relations[$strFullPath] = $config;
	}

	/**
	 * Returns the relation definition for $strClass by $strForeignClass
	 * If $strForeignClass is primaryKey or connection, @return will be a string.
	 * Otherwise, an array of parameters that will be fed into the Eloquent
	 * definition method will be returned.
	 *
	 * @api
	 * @param $strClassName
	 * @param $strForeignClass
	 * @return mixed
	 * @throws EloquentFactoryException
	 */
	public function getRelation($strClassName, $strForeignClass)
	{
		$strFullPath = $this->getFullyQualifiedClassName($strClassName);
		if(!isset($this->relations[$strFullPath]))
		{
			throw new EloquentFactoryException("No relation has been defined for $strFullPath.");
		}
		if(!isset($this->relations[$strFullPath][$strForeignClass]))
		{
			throw new EloquentFactoryException("No relation has been defined for $strFullPath->$strForeignClass.");
		}
		return $this->relations[$strFullPath][$strForeignClass];
	}

	/**
	 * Sets a relation accessor method alias.
	 * For example: If Account hasMany Files, the relation name might be File,
	 * but the accessor method might be "files".
	 *
	 * <pre>
	 * $f = new Factory();
	 * $f->setRelation("User","hasMany","File");
	 * $f->setRelationAccessorAlias("User","File","files");
	 * $user = $f->create("User");
	 * $files = User::find(1)->files();
	 * </pre>
	 *
	 * @api
	 * @param $strClassName
	 * @param $strRelationName
	 * @param $strAliasName
	 */
	public function setRelationAccessorAlias($strClassName, $strRelationName, $strAliasName)
	{
		$strFullPath = $this->getFullyQualifiedClassName($strClassName);
		$this->initRelation($strFullPath, $strRelationName);
		$this->relations[$strFullPath][$strRelationName]['relationAccessorAlias'] = $strAliasName;
	}

	/**
	 * Returns the accessor method alias that was set for the relation on the model class.
	 *
	 * @param $strClassName
	 * @param $strRelationName
	 * @return mixed string or FALSE if no alias is set for that relation
	 */
	public function getRelationAccessorAlias($strClassName, $strRelationName)
	{
		$strFullPath = $this->getFullyQualifiedClassName($strClassName);
		if(
			isset($this->relations[$strFullPath])
			&& isset($this->relations[$strFullPath][$strRelationName])
			&& isset($this->relations[$strFullPath][$strRelationName]['relationAccessorAlias'])
		)
		{
			return $this->relations[$strFullPath][$strRelationName]['relationAccessorAlias'];
		}

		return FALSE;
	}

	/**
	 * Uses the schema compiled up to the point this method is called to
	 * generate a PHP class file and include it. Then returns the class.
	 * Only generates the class file if it has not already been loaded.
	 *
	 * @param string $strClassName
	 * @param array $attributes attributes to fill the Eloquent model with
	 *
	 * @return object of type $strClassName, which extends Eloquent
	 */
	public function create($strClassName, array $attributes = array())
	{
		$strClassPath = $this->getFullyQualifiedClassName($strClassName);
		if(class_exists($strClassPath))
		{
			return new $strClassPath($attributes);
		}
		else
		{
			$strClassFileContents = $this->generateClass($strClassPath);

			$strFilePath = $this->getLoadableFilePath($strClassPath);

			// If the file is already created,
			// Then we don't need to write it again to the disk.
			if(!$this->setDirectoriesFor($strClassPath))
			{
				throw new EloquentFactoryException("Could not create the file structure required by the namespace.");
			}
			if(
				!is_writable(dirname($strFilePath))
				|| (file_exists($strFilePath) && !is_writable($strFilePath))
			)
			{
				throw new EloquentFactoryException("The place where the file would be written is not writable. Please check file permissions before continuing.");
			}
			if(!file_exists($strFilePath))
			{
				try {
					file_put_contents($strFilePath,$strClassFileContents);
				}
				catch (\Exception $e)
				{
					throw new EloquentFactoryException("Could not write file: $strFilePath. Check file and directory permissions for the target path.",5020, $e);
				}
			}

			if(is_readable($strFilePath))
			{
				require($strFilePath);
				if(class_exists($strClassPath))
				{
					if($this->bCleanupFiles)
					{
						unlink($strFilePath);
					}

					return new $strClassPath($attributes);
				}
				else
				{
					throw new EloquentFactoryException("There was a problem loading the class from the file system. I tried to read the file from: [$strFilePath].");
				}
			}

			throw new EloquentFactoryException("I could not read the temporary class file from the file system. I tried to read the file from: [$strFilePath]. Check file permissions maybe?");
		}
	}

	/**
	 * Returns the file path to the file that will be (or was) written to the file
	 * system and included to make the class available for instantiation.
	 *
	 * @param $strClassName
	 * @return string
	 */
	protected function getLoadableFilePath($strClassName)
	{
		$cache_path = $this->getStorageLocation();

		$strClass = $this->getNamespace($strClassName,TRUE);
		$strPath  = $this->getNamespace($strClassName);
		$strPath  = str_replace("\\",DIRECTORY_SEPARATOR,$strPath);
		$strPath  = $cache_path.DIRECTORY_SEPARATOR.$strPath.DIRECTORY_SEPARATOR."{$strClass}.php";

		return $strPath;
	}

	/**
	 * Returns the fully qualified class name for the given class.
	 * Assumes that if you're not passing in a fully qualified class name,
	 * the default namespace will be used.
	 *
	 * @param $strClassName
	 * @return string
	 */
	protected function getFullyQualifiedClassName($strClassName)
	{
		$strFullPath = $strClassName;
		if(preg_match('/\\\\/',$strFullPath) == 0)
		{
			$strFullPath = $this->getNamespace($strClassName).'\\'.$strClassName;
		}
		return $strFullPath;
	}

	/**
	 * Returns a string class definition to be loaded.
	 *
	 * @param $strClassName
	 * @return string class definition
	 */
	protected function generateClass($strClassName)
	{
		$this->validateRelations();
		$baseClass = $this->getBaseClass($strClassName);
		$strNamespace = $this->getNamespace($strClassName);

		$config = $this->getClassConfig($strClassName);

		$strConnection = "	protected \$connection = '" . $this->getConnection($strClassName) . "';";

		$strPrimaryKey = '';
		if(isset($config['primaryKey']) || $this->defaultPrimaryKey != 'id')
		{
			$strPrimaryKey = "	protected \$primaryKey = '{$this->getPrimaryKey($strClassName)}';";
		}

		$strRelations = $this->getRelationsAsString($config);

		$strClassName = $this->getNamespace($strClassName,TRUE);
		$strTable      = "	protected \$table = '" . $this->getTableName($strClassName) . "';";
		$strClass = "<?php
namespace $strNamespace;

class {$strClassName} extends $baseClass {

$strConnection
$strTable
$strPrimaryKey

$strRelations
}
";
		return $strClass;
	}

	/**
	 * Returns a string of function definitions to be used in the class definition.
	 *
	 * @param array $tableConfig array of table configuration information
	 *
	 * @return string relations defined for the class.
	 */
	protected function getRelationsAsString(array $tableConfig)
	{
		$strRet = '';

		$aValidMethods = $this->getValidRelationHandlers();
		foreach($tableConfig AS $strForeignClass => $relationConfig)
		{
			if(gettype($relationConfig) == 'array')
			{
				foreach($relationConfig AS $relationResolverMethod => $params)
				{
					if(in_array($relationResolverMethod,$aValidMethods))
					{
						foreach($params AS $index => $param)
						{
							if($param == '' || is_null($param))
							{
								$params[$index] = 'null';
							}
							else
							{
								$params[$index] = "'$param'";
							}
						}
						$relationAccessorMethod = (isset($tableConfig[$strForeignClass]['relationAccessorAlias'])) ? $tableConfig[$strForeignClass]['relationAccessorAlias']:strtolower($strForeignClass);
						$strParams = (count($params) > 0) ? ",".implode(",",$params):'';
						$strRet .= "	public function $relationAccessorMethod()\n";
						$strRet .= "	{\n";
						$strRet .= "		return \$this->$relationResolverMethod('$strForeignClass'$strParams);\n";
						$strRet .= "	}\n";
						$strRet .= "\n";
					}
				}
			}
		}

		return $strRet;
	}

	/**
	 * Returns the namespace base path for the class.
	 *
	 * @param $strClassName
	 * @param bool $bReturnClass
	 * @return string
	 */
	protected function getNamespace($strClassName,$bReturnClass = FALSE)
	{
		$iSlashPos = strrpos($strClassName,'\\');
		if($iSlashPos !== FALSE)
		{
			if($bReturnClass) return substr($strClassName,$iSlashPos+1);
			return substr($strClassName,0,$iSlashPos);
		}
		else
		{
			if($bReturnClass) return $strClassName;
			return $this->defaultNamespace;
		}
	}

	/**
	 * Sets the configuration array for the given table.
	 *
	 * @param string $strClassName
	 * @param array $config
	 */
	public function setClassConfig($strClassName, array $config)
	{
		$strClassName = $this->getFullyQualifiedClassName($strClassName);
		$this->initRelation($strClassName);

		$this->validateTableConfig($config);

		$this->relations[$strClassName] = $config;
	}

	/**
	 * Returns the configuration array for the given table.
	 *
	 * @param string $strClassName
	 */
	public function getClassConfig($strClassName)
	{
		$strClassName = $this->getFullyQualifiedClassName($strClassName);
		$this->initRelation($strClassName);
		return $this->relations[$strClassName];
	}

	/**
	 * Sets the schema for all the models that can be created
	 * by this factory.
	 *
	 * @param array $aSchema
	 */ 
	protected function validateRelations()
	{
		if(!is_array($this->relations))
		{
			throw new EloquentFactoryException("Relations isn't an array. This should never happen. Please report this bug to the Eloquent Factory developers, or submit a pull request with a bug fix in it.");
		}
		foreach($this->relations AS $strTable => $config)
		{
			$this->validateTableConfig($config);
		}

	}

	/**
	 * Returns an array of valid configuration field names.
	 *
	 * @return array
	 */
	protected function getValidConfigFields()
	{
		return array(
			'primaryKey'
			,'connection'
			,'baseclass'
			,'table'
		);
	}

	/**
	 * Returns an array of all the valid relation handler functions  on the Eloquent
	 * model abstract base class.
	 *
	 * @return array
	 */
	protected function getValidRelationHandlers()
	{
		return array(
			'hasOne'
			,'hasMany'
			,'hasManyThrough'
			,'belongsTo'
			,'belongsToMany'
			,'morphOne'
			,'morphMany'
			,'morphToMany'
			,'hasManyThrough'
		);

	}

	/**
	 * Validates the configuration array to make sure only allowed keys are defined.
	 *
	 * @param array $config
	 */
	protected function validateTableConfig($config)
	{
		$aStringValues = array();
		foreach($config AS $key => $value)
		{
			if(gettype($value) == 'string')
			{
				$aStringValues[$key] = $value;
			}
		}

		$diff = array_diff(array_keys($aStringValues), $this->getValidConfigFields());

		if(count($diff))
		{
			throw new EloquentFactoryException("The following values are not valid configuration values: [".implode(', ', $diff)."]");
		}

		foreach($config AS $key => $value)
		{
			if(gettype($value) == 'array')
			{
				foreach($value AS $strRelationConfigKey => $setting)
				{
					// Assumption:
					// If $setting is an array, then it is a relation definition
					if(gettype($setting) == 'array')
					{
						if(!in_array($strRelationConfigKey,$this->getValidRelationHandlers()))
						{
							throw new EloquentFactoryException("'" . $strRelationConfigKey . "' is not a valid relationResolver config setting.");
						}
					}
					else
					{
						if($strRelationConfigKey == 'relationAccessorAlias')
						{
							$this->validatePhpClassOrMethodName($setting);
						}
						// Other keys at this level should be validated here.
					}
				}
			}
		}

		return true;
	}

	/**
	 * Throws an error if the given name is not able to be used as a valid PHP
	 * method or class name.
	 *
	 * @param $strName
	 * @throws EloquentFactoryException
	 */
	protected function validatePhpClassOrMethodName($strName)
	{
		if(preg_match('/^[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*$/', $strName) === 0)
		{
			throw new EloquentFactoryException("[$strName] is not a valid PHP class or method name. It cannot be used to generate a class.");
		}
	}

	/**
	 * Initializes the relation array for the given class name.
	 *
	 * @param string $strClassName
	 * @param string $strRelationName
	 */
	protected function initRelation($strClassName, $strRelationName = null)
	{
		$strClassName = $this->getFullyQualifiedClassName($strClassName);
		if(
			!isset($this->relations[$strClassName]) ||
			!is_array($this->relations[$strClassName])
		)
		{
			$this->relations[$strClassName] = array();
		}

		if(!is_null($strRelationName))
		{
			if(!isset($this->relations[$strClassName][$strRelationName]))
			{
				$this->relations[$strClassName][$strRelationName] = array();
			}
		}
	}

	/**
	 * Recursively creates the directories required by the class path beginning at the storage location.
	 *
	 * @param $strClassName
	 * @throws EloquentFactoryException
	 * @return bool TRUE if the directory already exists or was successfully created.
	 * 			FALSE otherwise.
	 */
	protected function setDirectoriesFor($strClassName)
	{
		$strDirPath = $this->getNamespace($strClassName);
		$strDirPath = str_replace("\\",DIRECTORY_SEPARATOR,$strDirPath);
		$strStorageLocation = $this->getStorageLocation();
		$strDirPath = $strStorageLocation . DIRECTORY_SEPARATOR . $strDirPath;
		if(!is_dir($strDirPath))
		{
			try {
				mkdir($strDirPath,0777,TRUE);
				return true;
			}
			catch(\Exception $e)
			{
				throw new EloquentFactoryException("Could not create directory path for: $strDirPath. Check file permissions to make sure this process has access to create directories in the storage location: $strStorageLocation.",5020,$e);
			}
		}
		return true;
	}

}
