# Contribution Guide

This page contains guidelines for contributing to this package. Please review these guidelines before submitting any pull requests to the framework.

## Which Branch?

**ALL** bug fixes should be made to the 4.x branch which they belong. Bug fixes should never be sent to the `master` branch unless they fix features that exist only in the upcoming release.

## Pull Requests

The pull request process DOES NOT DIFFER for new features and bugs. Pull requests for bugs and features may be sent without creating any proposal issue. If you believe that you know of a solution for a bug that has been filed on Github, please leave a comment detailing your proposed fix.

### Feature Requests

If you have an idea for a new feature you would like to see added to Laravel, you may create an issue on Github with `[Request]` in the title. The feature request will then be reviewed by a core contributor.

## Coding Guidelines

This package follows the [PSR-0](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-0.md) and [PSR-1](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-1-basic-coding-standard.md) coding standards. In addition to these standards, below is a list of other coding standards that should be followed:

- Class opening `{` should be on the same line as the class name.
- Function and control structure opening `{` should be on a separate line.
- Interface names are suffixed with `Interface` (`FooInterface`)
- Tags and branch version numbers follow versioning standards set forth at http://semver.org.
