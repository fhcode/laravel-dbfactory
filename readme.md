## Laravel Eloquent ORM Factory

This Db Factory is intended for use with the Laravel 4 Eloquent ORM Framework. This wrapper provides a means of creating Eloquent Data Access Objects (or Models if that is your preferred word) at run-time based on a simple configuration set to identify what database connection is being used. Once the database connection identifier is set, creating objects is simple.

## Installation

Installation is supported through composer by adding the dependency to the "require" section of your composer.json file like so:

```javascript
	"require": {
		"laravel/dbfactory": "4.1.*"
	}
```

## Examples

```php
<?php

use Illuminate\Database\Eloquent\Factory;

$f = new Factory();

$f->setConnection('myConnection');

// Note: No class for 'User' is defined or loadable at this point.

$user = $f->create('User');

```

If you would like to define relationships inside a configuration array, you can do so easily:

```php
<?php

use Illuminate\Database\Eloquent\Factory;

$f = new Factory();

$f->setConnection('myConnection');
$f->setRelation('User','hasMany','Files','userID');

// Note: No class for 'User' is defined or loadable at this point.

$user = $f->create('User');

```

Or, you could define relations in an array.

```php
<?php

use Illuminate\Database\Eloquent\Factory;

$f = new Factory();

Factory::setConnection('myConnection');

$schema = array(
    'Illuminate\Database\Eloquent\Factory\Users' => array(
        'primaryKey' => 'userid'
        ,'connection' => $strConnection
        ,'Profile'    => array(
            'hasOne' => array('userid','userid'),
            'relationAccessorAlias' => "profile"
        )
        ,'File'   => array(
            'hasMany' => array('userid','userid1'),
            'relationAccessorAlias' => "files"
        )
    )
);


$f->setSchema($aSchema);

// Note: No class for 'Illuminate\Database\Eloquent\Factory\Users'
// is defined or loadable at this point.

$user = $f->create('Users');

echo "Your email address is: " . $user->profile()->email;

```

This wrapper is quite simple. I would very much welcome contributions, suggestions, ideas, etc. The concept has matured very quickly from a simple starting point to a robust and usable product.

## Defaults

By default, the table name is the same as the class name (to lower case). To change the table name, use Factory::setTable($strClass) or set the "table" attribute in the schema array.

By default, files will be generated and left alone in the storage location you give it. If you would like to have your factory classes automatically cleaned up (removed) from the file system, use the Factory::$bCleanupFiles public member variable. Be careful with this setting. If you have modified your generated classes and then turn file clean-up on, your modified class will disappear.

By default, if no namespace is specified for a class, the default namespace is: Illuminate\Database\Eloquent\Factory.
To change this, simply include the namespace you desire with the class name when you configure it with Factory::setRelation(), Factory::create(), or in the Factory::setSchema() array. You will need to be consistent in those three places with your namespace. If you use "Some\Namespace\User" in Factory::setRelation(), and then simply use "User" in Factory::create(), your "User" class will have the default namespace instead of "Some\Namespace\User". (Side-note: "Namespace" is a keyword that you can't actually use in a namespace. I used it for example purposes only.)

By default, all classes will extend \Illuminate\Database\Eloquent\Model. If you want your class to have a different base class, three ways to override this:

Global to this instance of the factory:

```php
$f->setBaseClass($strNewBaseClass);
```

Only for a specific class:

```php
$f->setBaseClass($strNewBaseClass,$strMyClass);

// OR

$f->setSchema(

$schema = array(
	'My\Name\Space\Users' => array(
		'baseclass'  => 'My\Path\To\A\Base\Class'
	)
)

);

// Note that the baseclass setting is on the same level
// as 'primarykey' and 'connection' in the examples above.

```

## Public Member Function & Variable Reference

```php
// By default, files will be written to the storage location
// and not cleaned up. Change this to true if you want your files
// removed.
public $bCleanupFiles = FALSE;

// This factory is not static. It is instance-based.
public function __construct()

// Set the starting point on the file system where files will be stored.
// Generated classes will be stored in their namespaced directories
// according to PSR-0 standars starting at the location specified here.
public function setStorageLocation($strPath)

// Returns the storage location. Defaults to storage_path('eloquent_cache')
public function getStorageLocation()

// Allows you to set a factory base class that all of your models will
// extend, or a base class for a specific model to extend.
// By default, all models extend Illuminate\Database\Eloquent\Model.
public function setBaseClass($mxdBaseClass, $strClassName = null)

// Returns the factory base class, or the base class for the given table.
public function getBaseClass($strClassName = null)

// Sets the table name that the model will be tied to.
public function setTableName($strClassName,$strTableName)

// Returns the table name that the model is tied to.
public function getTableName($strClassName)

// Sets the primary key for the model in question.
// If no class name is specified, the primary key is set for the whole factory
// for all classes generated by this factory that don't have a specific
// primary key set.
public function setPrimaryKey($primaryKey,$strClassName = null)

// Returns the factory primary key, or the primary key for the given table.
public function getPrimaryKey($strClassName = null)

// Sets the factory connection if no class name is given,
// or sets the connection that a specific class will use.
public function setConnection($strConnectionName,$strClassName = null)

// Returns the factory connection name or the connection for a particular class.
public function getConnection($strClassName = null)

// Sets all class schema settings with the settings found in the given array.
// The array schema with all possible values is below in the documentation.
public function setSchema($aSchema)

// Returns the entire schema for all classes defined in this factory.
public function getSchema()

// Sets a relation for a particular class.
// $strType must be a valid Eloquent model method that returns a relation object,
// such as 'hasOne' or 'hasMany'. All eloquent relation methods are supported.
// $strRelationAccessorAlias is the name of the method you want to use to access
// that relation from your model. For example, if User hasMany Profile, and you
// want to access your profiles with '$user->account_profiles()' instead of
// '$user->profiles()', use this setting to tell the factory what method name
// you want to use.
public function setRelation($strClassName, $strType, $strForeignTable, $strForeignKey = null, $strLocalKey = null, $strRelationAccessorAlias = null)

// Returns the relation definition array for a particular class' relation.
public function getRelation($strClassName, $strForeignClass)

// Sets the relation accessor method name as described above setRelation()
public function setRelationAccessorAlias($strClassName, $strRelationName, $strAliasName)

// Returns the relation accessor name as described above setRelation()
public function getRelationAccessorAlias($strClassName, $strRelationName)

// This is where the magic happens. This method Creates the class, writes it to
// the file system, includes it, instantiates the class, and returns it.
// If the file already exists on the file system, or the class is already
// included, no file generation happens and the class is simply instantiated
// and returned.
public function create($strClassName, array $attributes = array())

// Allows you to replace a single class configuration within the factory
// schema. This allows you to replace schemas for a single class at a time
// rather than having to replace the entire schema for all classes.
public function setClassConfig($strClassName, array $config)

// Returns a single class configuration from the factory schema.
public function getClassConfig($strClassName)

```

## What this library is not

This wrapper is not intended to replace complex class definitions. In general, your model should contain the business logic that's related to it in a class file that is actually written to the file system and checked in to source code somewhere. This factory is a short-cut for those times where you don't want to have to create a hundred different files in order to access your data tables.

## What this library is

What this library is useful for is generating those classes that you need on the file system as you need them in real-time. You can specify the basic configuration of relationships and settings for your classes, and then the files that you can customize will magically appear on the file system as you are developing the code.

There is no need to change your factory calls after the files are generated because the factory checks to see if the file exists first and uses that if it's already defined.

## Contributing

Thank you for considering contributing. This package will be versioned along with the rest of the laravel framework. Therefore the 4.1 tag is intended to be compatible with the Laravel Framework version 4.1. If a breaking change happens in the Laravel Framework, please submit a pull request to the 4.1 branch and suggest that your fix might make it compatible with the latest version of the framework.
If you are submitting a bug-fix, or an enhancement that is **not** a breaking change, submit your pull request to the branch corresponding to the latest stable release of the framework, such as the `4.1` branch. If you are submitting a breaking change or an entirely new component, submit your pull request to the `master` branch.

### License

Eloquent Db Factory is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
