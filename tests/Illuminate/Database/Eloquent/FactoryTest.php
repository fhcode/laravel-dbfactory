<?php namespace Illuminate\Database\Eloquent;

use Mockery as m;
use Orchestra\Testbench\TestCase;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\FactoryInstanceWrapper AS Factory;

/**
 * IMPORTANT NOTE TO DEVELOPERS:
 *
 * These unit tests use the FactoryInstanceWrapper class above, which exposes
 * all of the protected member variables and functions so I can write more specific
 * unit tests. These unit tests should be used for a reference with that in mind.
 * If you are not able to access a particular factory method or property because
 * it is protected, please use the public getter or setter instead.
 *
 * See the readme.md file for a list of public properties and methods.
 */

/*
if(!function_exists('storage_path'))
{
*/
function storage_path($path)
	{
		$slash = DIRECTORY_SEPARATOR;
		$strTmp = "{$slash}tmp";
		$path = "$strTmp$slash$path";
		if(!is_dir($path))
		{
			if(is_writable(dirname($path)))
			{
				mkdir($path);
			}
		}
		return $path;
	}
/*
}
*/

class FactoryTest extends TestCase {

	public function testNamespaceParser()
	{
		$f = new Factory();
		$this->assertEquals("Some\\Special\\Path",$f->getNamespace("Some\\Special\\Path\\Class"));
		$this->assertEquals("Class",$f->getNamespace("Some\\Special\\Path\\Class",TRUE));
	}

	public function testConnectionSetter()
	{
		$f = new Factory();
		$strConnection = "TestConnection";
		$f->setConnection($strConnection);
		$this->assertEquals($strConnection,$f->getConnection());

		$strUserConnection = 'UserConnection';
		$f->setConnection($strUserConnection,'User');
		$this->assertEquals($strUserConnection,$f->getConnection('User'));
	}

	public function testSetSchemaException()
	{
		$f = new Factory();
		$schema = array(
			'Users' => array(
				'ThisShouldCauseAnException' => ''
			)
		);
		$this->setExpectedException("\\Illuminate\\Database\\Eloquent\\EloquentFactoryException");
		$f->setSchema($schema);
	}

	public function testSetSchema()
	{
		$f = new Factory();
		$strConnection = "TestConnection";
		$schema = array(
			'Illuminate\Database\Eloquent\Factory\Users' => array(
				'primaryKey' => 'userid'
				,'connection' => $strConnection
				,'Profiles'    => array(
					'hasOne' => array('userid','userid'),
					'relationAccessorAlias' => "profiles"
				)
				,'Files'   => array(
					'hasMany' => array('userid','userid1'),
					'relationAccessorAlias' => "profiles"
				)
			)
		);
		$f->setSchema($schema);
		$this->assertEquals($schema,$f->getSchema());

		$f = new Factory();
		$f->setClassConfig('Users',$schema['Illuminate\Database\Eloquent\Factory\Users']);
		$this->assertEquals($schema,$f->getSchema());
	}

	public function testRelationSetter()
	{
		$f = new Factory();
		$f->setRelation("User", "hasOne", "Thing", "thing_id", "user_id");

		$strExpected = "Array
(
    [hasOne] => Array
        (
            [0] => thing_id
            [1] => user_id
        )

)
";

		$strActual = print_r($f->getRelation("User","Thing"),1);
		$this->assertEquals($strExpected,$strActual);
	}

	public function testGenerateClass()
	{
		$f = new Factory();
		$f->setConnection("TestConnection");
		$f->setPrimaryKey('user_id',"User");
		$f->setRelation("User", "hasOne", "Thing", "thing_id", "user_id");
		$f->setRelation("User", "hasMany", "Files", "file_id", "user_id");
		$strExpected = "<?php
namespace Illuminate\\Database\\Eloquent\\Factory;

class User extends \\Illuminate\\Database\\Eloquent\\Model {

	protected \$connection = 'TestConnection';
	protected \$table = 'user';
	protected \$primaryKey = 'user_id';

	public function thing()
	{
		return \$this->hasOne('Thing','thing_id','user_id');
	}

	public function files()
	{
		return \$this->hasMany('Files','file_id','user_id');
	}


}
";
		$this->assertEquals($strExpected,$f->generateClass("User"));
	}

	public function testGenerateClassWithNamespace()
	{
		$f = new Factory();
		$f->setConnection("TestConnection");
		$strUserClassWithNamespace = "Special\\Path\\Path\\User";
		$f->setPrimaryKey('user_id',$strUserClassWithNamespace);
		$f->setRelation($strUserClassWithNamespace, "hasOne", "Thing", "thing_id", "user_id");
		$f->setRelation($strUserClassWithNamespace, "hasMany", "Files", "file_id", "user_id");
		$strExpected = "<?php
namespace Special\\Path\\Path;

class User extends \\Illuminate\\Database\\Eloquent\\Model {

	protected \$connection = 'TestConnection';
	protected \$table = 'user';
	protected \$primaryKey = 'user_id';

	public function thing()
	{
		return \$this->hasOne('Thing','thing_id','user_id');
	}

	public function files()
	{
		return \$this->hasMany('Files','file_id','user_id');
	}


}
";
		$this->assertEquals($strExpected,$f->generateClass($strUserClassWithNamespace));
	}

	public function testGenerateClassWithAlternateBaseClass()
	{
		$f = new Factory();
		$f->setConnection("TestConnection");
		$f->setBaseClass("Illuminate\\Database\\Eloquent\\Model");
		$strUserClassWithNamespace = "Special\\Path\\Path\\User";
		$f->setPrimaryKey('user_id',$strUserClassWithNamespace);
		$f->setRelation($strUserClassWithNamespace, "hasOne", "Thing", "thing_id", "user_id");
		$f->setRelation($strUserClassWithNamespace, "hasMany", "Files", "file_id", "user_id");
		$strExpected = "<?php
namespace Special\\Path\\Path;

class User extends \\Illuminate\\Database\\Eloquent\\Model {

	protected \$connection = 'TestConnection';
	protected \$table = 'user';
	protected \$primaryKey = 'user_id';

	public function thing()
	{
		return \$this->hasOne('Thing','thing_id','user_id');
	}

	public function files()
	{
		return \$this->hasMany('Files','file_id','user_id');
	}


}
";
		$this->assertEquals($strExpected,$f->generateClass($strUserClassWithNamespace));
	}

	public function testGenerateClassWithAlternateBaseClassForSpecificTable()
	{
		$f = new Factory();
		$f->setConnection("TestConnection");
		$strUserClassWithNamespace = "Special\\Path\\Path\\User";
		$f->setBaseClass("\\Illuminate\\Database\\Eloquent\\Model",$strUserClassWithNamespace);
		$f->setPrimaryKey('user_id',$strUserClassWithNamespace);
		$f->setRelation($strUserClassWithNamespace, "hasOne", "Thing", "thing_id", "user_id");
		$f->setRelation($strUserClassWithNamespace, "hasMany", "Files", "file_id", "user_id");
		$strExpected = "<?php
namespace Special\\Path\\Path;

class User extends \\Illuminate\\Database\\Eloquent\\Model {

	protected \$connection = 'TestConnection';
	protected \$table = 'user';
	protected \$primaryKey = 'user_id';

	public function thing()
	{
		return \$this->hasOne('Thing','thing_id','user_id');
	}

	public function files()
	{
		return \$this->hasMany('Files','file_id','user_id');
	}


}
";
		$this->assertEquals($strExpected,$f->generateClass($strUserClassWithNamespace));
	}

	public function testSetBaseClassException()
	{
		$f = new Factory();
		$f->setConnection("TestConnection");
		$strUserClassWithNamespace = "Special\\Path\\Path\\User";
		$this->setExpectedException("\\Illuminate\\Database\\Eloquent\\EloquentFactoryException");
		$f->setBaseClass(4,$strUserClassWithNamespace);
	}

	public function testSetBaseClassWithObject()
	{
		$f = new Factory();
		$f->bCleanupFiles = TRUE;
		$f->setConnection("TestConnection");
		$strUserClassWithNamespace = "Special\\Path\\Path\\User";
		$user = $f->create($strUserClassWithNamespace);

		$f = new Factory();
		$f->bCleanupFiles = TRUE;
		$f->setConnection("TestConnection");
		$strAccountClassWithNamespace = "Special\\Path\\Path\\Account";
		$f->setBaseClass($user,$strAccountClassWithNamespace);
		$account = $f->create($strAccountClassWithNamespace);
		$this->assertEquals($strUserClassWithNamespace,get_parent_class($account));
	}

	public function testClassLoader()
	{
		$f = new Factory();
		$f->bCleanupFiles = TRUE;
		$f->setConnection('TestConnection');
		$strUserClassWithNamespace = "Another\\Space\\Path\\User";
		$f->setRelation($strUserClassWithNamespace, "hasOne", "Thing", "thing_id", "user_id");
		$f->setRelation($strUserClassWithNamespace, "hasMany", "Files", "file_id", "user_id");

		$user = $f->create($strUserClassWithNamespace);

		$this->assertEquals($strUserClassWithNamespace,get_class($user));
	}

	public function testGetBaseClass()
	{
		$f = new Factory();
		$strBaseClass = "\\Illuminate\\Database\\Eloquent\\Model";
		$strTableBaseClass = "\\Illuminate\\Database\\Eloquent\\Model";
		$f->setBaseClass($strBaseClass);
		$f->setBaseClass($strTableBaseClass,'User');

		$this->assertEquals($strBaseClass,$f->getBaseClass());
		$this->assertEquals($strTableBaseClass,$f->getBaseClass('User'));
	}

	public function testGetPrimaryKey()
	{
		$f = new Factory();
		$strPrimaryKey = "user_id";
		$f->setPrimaryKey($strPrimaryKey,'Account');

		$this->assertEquals($strPrimaryKey,$f->getPrimaryKey('Account'));

		$strPrimaryKey = "id";

		$f = new Factory();
		$this->assertEquals($strPrimaryKey,$f->getPrimaryKey('Account'));

		$f = new Factory();
		$f->bCleanupFiles = TRUE;
		$f->setConnection('TestConnection');
		$account = $f->create('Account');
		$this->assertEquals($strPrimaryKey,$f->getPrimaryKey('Account'));
		$this->assertEquals('TestConnection',$f->getConnection('Account'));
		$anotherAccount = $f->create('Account');
	}

	public function testUndefinedClassException()
	{
		$f = new Factory();
		$this->setExpectedException('Illuminate\\Database\\Eloquent\\EloquentFactoryException');
		$class = $f->getRelation('UndefinedClass','Thing');
	}

	public function testUndefinedRelationException()
	{
		$f = new Factory();
		$this->setExpectedException('Illuminate\\Database\\Eloquent\\EloquentFactoryException');
		$class = $f->getRelation('UndefinedClass','AnotherClass');
	}

	public function testInvalidRelationException()
	{
		$f = new Factory();
		$f->bCleanupFiles = TRUE;
		$this->setExpectedException('Illuminate\\Database\\Eloquent\\EloquentFactoryException');
		$f->setRelation("UndefinedClass",'hasStuff','AnotherClass');
	}

	public function testForeignAndLocalKeys()
	{
		$f = new Factory();
		$f->setConnection("TestConnection");
		$f->setRelation("User","hasMany","File",null,'file_id','files');

		$this->assertEquals("files",$f->getRelationAccessorAlias("User","File"));

		$strExpected = "<?php
namespace Illuminate\\Database\\Eloquent\\Factory;

class User extends \\Illuminate\\Database\\Eloquent\\Model {

	protected \$connection = 'TestConnection';
	protected \$table = 'user';


	public function files()
	{
		return \$this->hasMany('File',null,'file_id');
	}


}
";

		$this->assertEquals($strExpected,$f->generateClass("User"));
	}

	public function testRelationAccessorAlias()
	{
		$f = new Factory();
		$f->setConnection("TestConnection");
		$f->setRelation("User","hasMany","File");
		$f->setRelationAccessorAlias("User","File","files");

		$this->assertEquals("files",$f->getRelationAccessorAlias("User","File"));
		$this->assertFalse($f->getRelationAccessorAlias("Foo","Bar"));

		$strExpected = "<?php
namespace Illuminate\\Database\\Eloquent\\Factory;

class User extends \\Illuminate\\Database\\Eloquent\\Model {

	protected \$connection = 'TestConnection';
	protected \$table = 'user';


	public function files()
	{
		return \$this->hasMany('File');
	}


}
";

		$this->assertEquals($strExpected,$f->generateClass("User"));
	}

	public function testSetTableNameUnlikeClassName()
	{
		$f = new Factory();
		$f->bCleanupFiles = TRUE;
		$f->setConnection("TestConnection");
		$f->setRelation("User","hasMany","File");
		$f->setTableName("User","accounts");
		$f->setRelationAccessorAlias("User","File","files");

		$this->assertEquals("files",$f->getRelationAccessorAlias("User","File"));

		$strExpected = "<?php
namespace Illuminate\\Database\\Eloquent\\Factory;

class User extends \\Illuminate\\Database\\Eloquent\\Model {

	protected \$connection = 'TestConnection';
	protected \$table = 'accounts';


	public function files()
	{
		return \$this->hasMany('File');
	}


}
";

		$this->assertEquals($strExpected,$f->generateClass("User"));

		$f->create("User");

		$this->assertEquals('accounts',$f->getTableName('User'));
	}

	public function testDefaultPrimaryKey()
	{
		$a = new Factory();
		$this->assertEquals('id',$a->getPrimaryKey());

		$strExpected = "<?php
namespace Illuminate\\Database\\Eloquent\\Factory;

class SomethingElse extends \\Illuminate\\Database\\Eloquent\\Model {

	protected \$connection = '';
	protected \$table = 'something_else';



}
";

		$strActual = $a->generateClass("SomethingElse");
		$this->assertEquals($strExpected,$strActual);

		$a->setPrimaryKey('some_id');

		$strExpected = "<?php
namespace Illuminate\\Database\\Eloquent\\Factory;

class SomethingElse extends \\Illuminate\\Database\\Eloquent\\Model {

	protected \$connection = '';
	protected \$table = 'something_else';
	protected \$primaryKey = 'some_id';


}
";
		$strActual = $a->generateClass("SomethingElse");
		$this->assertEquals($strExpected,$strActual);
	}

	public function testGetRelationThatDoesntExist()
	{
		$f = new Factory();

		$f->setRElation("Football","hasMany","Lace","ball_id","lace_id",'laces');
		$this->setExpectedException("\\Illuminate\\Database\\Eloquent\\EloquentFactoryException");
		$f->getRelation("Football","Licorice");
	}

	public function testCodeCleanup()
	{
		$f = new Factory();
		$f->bCleanupFiles = TRUE;

		$f->setRElation("Football","hasMany","Lace","ball_id","lace_id",'laces');
		$f->create("Football");

		$strFilePath = $f->getLoadableFilePath("Football");

		$this->assertFalse(file_exists($strFilePath));

	}

	public function testDefaultStorageLocation()
	{
		$f = new Factory();

		$strStoragePath = storage_path('eloquent_cache');
		$this->assertEquals($strStoragePath,$f->getStorageLocation());
	}

	public function testSetStorageLocation()
	{
		$f = new Factory();

		$strStorageLocation = "/flippygoldfishfolder";
		$f->setStorageLocation($strStorageLocation);

		$this->assertEquals($strStorageLocation,$f->getStorageLocation());

	}

	public function testRelationsIsNotAnArray()
	{
		$f = new Factory();
		$f->bCleanupFiles = TRUE;
		$f->relations = '';

		$this->setExpectedException("\\Illuminate\\Database\\Eloquent\\EloquentFactoryException");
		$f->create("TestClass");
	}

	public function testFileCantBeWrittenException()
	{
		$f = new Factory();
		$f->setStorageLocation('/');
		$this->setExpectedException("\\Illuminate\\Database\\Eloquent\\EloquentFactoryException");
		$f->create("ThisClassCantBeWritten");
	}

	public function testBadAccessorName()
	{
		$f = new Factory();
		$this->setExpectedException("\\Illuminate\\Database\\Eloquent\\EloquentFactoryException");
		$f->setRelation("ClassWithBadAccessorMethodName",'hasOne','AnotherClass','id','id','accessor&method');
	}

}
