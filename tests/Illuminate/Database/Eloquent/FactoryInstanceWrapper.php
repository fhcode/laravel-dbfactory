<?php
/**
 * Created by PhpStorm.
 * User: jwatson
 * Date: 3/6/14
 * Time: 10:51 PM
 */

namespace Illuminate\Database\Eloquent;

use ReflectionClass;
use ReflectionMethod;
use ReflectionProperty;

class FactoryInstanceWrapper extends Factory {

	private $aProtectedMethodNames = array();
	private $aProtectedPropertyNames  = array();

	protected function getProtectedMethodNames()
	{
		if($this->aProtectedMethodNames)
		{
			return $this->aProtectedMethodNames;
		}

		$reflector = new \ReflectionClass($this);
		$aMethods = $reflector->getMethods(ReflectionMethod::IS_PROTECTED);
		$aProtectedMethods = array();
		foreach($aMethods AS $methodObject)
		{
			$aProtectedMethods[] = $methodObject->name;
		}

		return $this->aProtectedMethodNames = $aProtectedMethods;
	}

	protected function getProtectedPropertyNames()
	{
		if($this->aProtectedPropertyNames)
		{
			return $this->aProtectedPropertyNames;
		}

		$reflector = new \ReflectionClass($this);
		$aProperties = $reflector->getProperties(ReflectionProperty::IS_PROTECTED);
		$aProtectedProperties = array();
		foreach($aProperties AS $propertyObject)
		{
			$aProtectedProperties[] = $propertyObject->name;
		}

		return $this->aProtectedPropertyNames = $aProtectedProperties;
	}
	/**
	 * Make all protected methods public
	 *
	 * @param $name
	 * @param $arguments
	 *
	 * @return mixed
	 */
	public function __call($name, $arguments)
	{
		$aNames = $this->getProtectedMethodNames();
		if(in_array($name,$aNames))
		{
			return call_user_func_array(array($this,$name),$arguments);
		}
	}

	/**
	 * Returns the protected member value
	 *
	 * @param $name
	 * @return mixed
	 * @throws EloquentFactoryException
	 */
	public function __get($name)
	{
		$aNames = $this->getProtectedPropertyNames();
		if(in_array($name,$aNames))
		{
			return $this->$name;
		}

		throw new EloquentFactoryException("Trying to get a property that doesn't exist on the Factory.");
	}

	/**
	 * Sets the protected member value
	 *
	 * @param $name
	 * @param $value
	 * @throws EloquentFactoryException
	 */
	public function __set($name,$value)
	{
		$aNames = $this->getProtectedPropertyNames();
		if(in_array($name,$aNames))
		{
			$this->$name = $value;
			return;
		}

		throw new EloquentFactoryException("Trying to set a property that doesn't exist on the Factory.");
	}
}